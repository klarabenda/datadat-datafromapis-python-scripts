import requests
import pandas as pd
import xlsxwriter
import json
import time
import csv


fbDataFrame = pd.DataFrame([{'name':'',
                             'country':'',                                                
                             'legislature_name':'',
                             'lastmod_date': '',
                             'person_count': '',
                             'type': '',
                             'period_id': '',
                             'period_name': '',
                             'start_date': '',
                             'end_date': '',
                             'csv_url':''}])

url='https://raw.githubusercontent.com/everypolitician/everypolitician-data/master/countries.json'
r = requests.get(url)
rawdata = r.json()
for place in rawdata:
    name=''
    country=''
    legislatures=''
    if 'name' in place:
        name=place['name']
    if 'country' in place:
        country=place['country']
    if 'legislatures' in place:
        legislatures=place['legislatures']
        for thing in legislatures:
            legislature_name=''
            lastmod=''
            lastmod_date=''
            legislative_periods=''
            if 'name' in thing:
                legislature_name=thing['name']
            if 'type' in thing:
                type=thing['type']
            if 'person_count' in thing:
                person_count=thing['person_count']
            if 'lastmod' in thing:
                lastmod = int(thing['lastmod'])
                lastmod_date=time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(lastmod))
            if 'legislative_periods' in thing:
                legislative_periods = thing['legislative_periods']
                for period in legislative_periods:
                    period_id=''
                    period_name=''
                    start_date=''
                    end_date=''
                    csv_url=''
                    if 'id' in period:
                        period_id = period['id']
                    if 'name' in period:
                        period_name = period['name']
                    if 'start_date' in period:
                        start_date = period['start_date']
                    if 'end_date' in period:
                        end_date = period['end_date']
                    if 'csv_url' in period:
                        csv_url=period['csv_url']
                    fbDataFrame = fbDataFrame.append([{  'name':name,
                                                         'country':country,                                                
                                                         'legislature_name':legislature_name,
                                                         'lastmod_date': lastmod_date,
                                                         'person_count': person_count,
                                                         'type': type,
                                                         'period_id': period_id,
                                                         'period_name': period_name,
                                                         'start_date': start_date,
                                                         'end_date': end_date,
                                                         'csv_url':csv_url}], ignore_index=True)

print('data ready')
filename='everypolitician_data.xlsx'
writer = pd.ExcelWriter(filename, engine='xlsxwriter')
fbDataFrame.to_excel(writer, sheet_name='Sheet1')
writer.save()  
print('xls ready')



#Open term
url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Abkhazia/Assembly/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/1_Abkhazia_Peoples-Assembly_5th-Convocation_35persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/55e3ee76bf4ceabe9168f8b055cc18cac551c322/data/Aland/Lagting/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/745_Åland_Lagting_2015–2019_60persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Alderney/States/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/5_Alderney_States_2017-2018_14persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/a351051db91419991a4c6d9645cd3b7c5fe0a7e4/data/Algeria/Majlis/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/7_Algeria_Peoples-National-Assembly_7th-National-Assembly_478persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9c714427c4569e46ed69ee999c3a7b44a50e0337/data/American_Samoa/House/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/8_American-Samoa_House-of-Representatives_2015–_17persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Andorra/General_Council/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/9_Andorra_Consell-General_2015_31persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/16fd5a287a62949aa5a32c3efd2362d4d881742d/data/Angola/National_Assembly/term-4.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/10_Angola_National-Assembly_4th-National-Assembly_343persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/bee90cf27d0a1a8ed83325de9500ed8dfd8e6361/data/Anguilla/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/12_Anguilla_House-of-Assembly_2015–2020_18persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Antigua_and_Barbuda/Representatives/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/19_Antigua-and-Barbuda_House-of-Representatives_2014–2019_63persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/2741c99e06977634b526fd0a0cb8129ee41007c9/data/Armenia/Assembly/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/35_Armenia_National-Assembly_6th-Convocation_186persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Aruba/Estates/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/37_Aruba_Estates-of-Aruba_7th-Aruban-Estates_21persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/d24625f9d3a1c521401b0466698442ecf8e10625/data/Australia/Representatives/term-45.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/38_Australia_House-of-Representatives_45th-Parliament_514persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/acbeb50b4414d0e469d18b67643134907a328de7/data/Australia/Senate/term-45.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/49_Australia_Senate_45th-Parliament_270persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/8013fb3188dca8212f3d300836943b091509cb7e/data/Azerbaijan/National_Assembly/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/61_Azerbaijan_National-Assembly_5th-Convocation-of-the-National-Assembly-of-Azerbaijan_152persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Bahamas/House_of_Assembly/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/63_Bahamas_House-of-Assembly_2012–_38persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Bahrain/Council_of_Representatives/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/64_Bahrain_Council-of-Representatives_2014–_40persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/b8d332475935317d5d800f3ddd78c9e8cfc5648c/data/Bangladesh/House/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/65_Bangladesh_Jatiyo-Sangshad_10th-Parliament_550persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Barbados/House_of_Assembly/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/67_Barbados_House-of-Assembly_2013–2018_30persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/896dd3929f7d97d615a583674eabc1c7345cd2c6/data/Belarus/Chamber/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/68_Belarus_House-of-Representatives_6th-Convocation_192persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/d9b2131c51cc3ac6aebfd9df679b0418b0fd999c/data/Belgium/Representatives/term-54.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/70_Belgium_Chamber-of-Representatives_54e-législature_178persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9432bf29041cb6966db27e1db78bc4dbf9313dc1/data/Belize/Representatives/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/71_Belize_House-of-Representatives_10th-House-of-Representatives_41persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/43504c7e3d57a675e837f83f6242d69b2cfeba15/data/Benin/National_Assembly/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/73_Benin_Assemblée-Nationale_7è-législature_83persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/6efd2607a68de493082ccb8a375c82d963546164/data/Bermuda/Assembly/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/74_Bermuda_Parliament_2017–_48persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/dc1437513409c82e635789367ccd091386d3b9e2/data/Bhutan/Assembly/term-2.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/76_Bhutan_National-Assembly_2nd-Parliament_48persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Bolivia/Deputies/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/77_Bolivia_Chamber-of-Deputies_2015–_130persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Bosnia_and_Herzegovina/House_of_Representatives/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/78_Bosnia-and-Herzegovina_House-of-Representatives_2014–_42persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/492f1b71994d6b6e3616ba40ae01dfb6580d4e87/data/Botswana/Assembly/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/79_Botswana_National-Assembly_11th-Parliament-of-Botswana_64persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e8ea05eef0bf7f31aa9d2b1f8d44b9486b7b4567/data/Brazil/Deputies/term-55.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/80_Brazil_Chamber-of-Deputies_55th-Chamber-of-Deputies-of-Brazil_942persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/British_Virgin_Islands/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/82_British-Virgin-Islands_House-of-Assembly_3rd-Assembly_23persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/135c7645f45525800f34ea46a37593cd05cc8ac9/data/Bulgaria/National_Assembly/term-44.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/95_Bulgaria_National-Assembly_44th-National-Assembly_1106persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Burkina_Faso/Assembly/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/101_Burkina-Faso_Assemblée-Nationale_VIIe-législature_476persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Burundi/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/106_Burundi_Assemblée-nationale_2015–_227persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Cabo_Verde/Assembly/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/108_Cabo-Verde_Assembleia-Nacional_IX-Legislatura_131persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1e438168c0807023e0c155120f4ffb83957222b0/data/Cambodia/National_Assembly/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/110_Cambodia_National-Assembly_5th-Mandate_123persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Cameroon/Assembly/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/111_Cameroon_Assemblée-Nationale_9e-législature_964persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Cameroon/Senate/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/119_Cameroon_Sénat_9e-législature_100persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/0ac300b73b07c77ce218bf5575b351ba54841009/data/Canada/Commons/term-42.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/120_Canada_House-of-Commons_42nd-Canadian-Parliament_550persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/cbc38c140c4ff61ea8909d402f610c31ad0cdd71/data/Canada/Senate/term-42.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/122_Canada_Senate_42nd-Parliament_116persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Cayman_Islands/Legislative_Assembly/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/123_Cayman-Islands_Legislative-Assembly_2013–2017_18persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Chad/Assembly/term-3.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/124_Chad_Assemblée-Nationale_Troisième-Législature_191persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/China/Congress/term-12.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/132_China_National-People’s-Congress_12th-National-People’s-Congress_2956persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Comoros/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/135_Comoros_Assembly-of-the-Union_2015–_24persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Congo-Brazzaville/Assembly/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/136_Congo-Brazzaville_Assemblee-Nationale_13ème-législature_122persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/a3da8479d41774bf179381629c7d14edc5712707/data/Congo-Kinshasa/Assembly/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/137_Congo-Kinshasa-(DRC)_National-Assembly_2012–_498persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/d15ea55b75488ae898227235d82539635b55a834/data/Cook_Islands/Parliament/term-14.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/138_Cook-Islands_Parliament_14th-Parliament_49persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Ivory_Coast/Assembly/term-2.2.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/155_Côte-dIvoire_National-Assembly_2e-législature-de-la-2e-République_241persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Croatia/Sabor/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/142_Croatia_Sabor_9th-Sabor_354persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Cyprus/House_of_Representatives/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/146_Cyprus_House-of-Representatives_11th-Term_91persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/dbc9ab53890f0eeed91c363f82d3870d9f3c55ae/data/Czech_Republic/Deputies/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/148_Czech-Republic_Chamber-of-Deputies_7.-volební-období_892persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/412c0537ac7806062f77016754a40895cff5a681/data/Denmark/Folketing/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/156_Denmark_Folketing_Folketing-2015–_613persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Djibouti/Assembly/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/164_Djibouti_National-Assembly_6th-National-Assembly_65persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Dominica/House_of_Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/165_Dominica_House-of-Assembly_2014–_21persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/02fc9a2f8fda520af85409a41299a95206ba3286/data/Egypt/Parliament/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/168_Egypt_Parliament_2015–_600persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/El_Salvador/Legislative_Assembly/term-2015-2018.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/169_El-Salvador_Legislative-Assembly_2015–2018_84persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9fb37fbb01783e1d14520fed10619959e5814e32/data/Estonia/Riigikogu/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/170_Estonia_Riigikogu_13th-Riigikogu_214persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/66e89162aa3b6b30193c685bb88de06a2de007ff/data/Faroe_Islands/Logting/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/182_Faroe-Islands_Løgting_Løgting-2015–_103persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Fiji/Parliament/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/190_Fiji_Parliament_2014_54persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e801cd0425b64ed7dbc789483f0012d751341051/data/Finland/Eduskunta/term-37.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/191_Finland_Eduskunta_37th-Parliament-of-Finland_500persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/25931f5f603a8a34d879db9e2b60a12eac775a9c/data/France/National_Assembly/term-15.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/204_France_Assemblée-nationale_XVe-législature-de-la-Ve-République_1552persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/French_Polynesia/Assembly/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/208_French-Polynesia_Assembly_2013–_59persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/4490bdad418d5e82f4716b54d009507c919c7d80/data/Gabon/Assembly/term-12.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/209_Gabon_Assemblée-nationale_12-Législature_114persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Gambia/National_Assembly/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/210_Gambia_National-Assembly_2012–_53persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Georgia/Parliament/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/211_Georgia_Parliament-of-Georgia_9th-Convocation_249persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/225837259784522f5fcc4c246ddebea943f85947/data/Germany/Bundestag/term-19.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/213_Germany_Bundestag_19th-Bundestag_4073persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/078a15832d9a990740b955ca7e45794152b6af01/data/Ghana/Parliament/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/232_Ghana_Parliament_Seventh-Parliament-of-the-Fourth-Republic_413persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1d7d4a5c7c40594c5f0b0b7c48b38d9bdcf80759/data/Greece/Parliament/term-17.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/246_Greece_Hellenic-Parliament_17th-Hellenic-Parliament_1783persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/ab68272e9c69984e9891854cde86e8d3365c3123/data/Greenland/Inatsisartut/term-12.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/263_Greenland_Inatsisartut_Inatsisartut-12_161persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Grenada/House_of_Representatives/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/275_Grenada_House-of-Representatives_2013_15persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Guam/Parliament/term-33.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/276_Guam_Parliament_33rd-Guam-Legislature_28persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/8f9d5bb62948d2504adb7ce7c8c2196ddaadddb9/data/Guatemala/Congress/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/280_Guatemala_Congress_8th-legislature-of-Guatemala_255persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/64728e52c3c84398eb3b1f5c4c9c57c627e46ab4/data/Guernsey/States/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/282_Guernsey_States_2016–_68persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Guinea-Bissau/Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/284_Guinea-Bissau_National-Peoples-Assembly_2014-_100persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Guyana/National_Assembly/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/285_Guyana_National-Assembly_11th-Parliament_64persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Honduras/Congreso_Nacional/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/287_Honduras_National-Congress_VIII-Legislatura_131persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/4c7e7801b72ffefa9c1862179401001dc0ce2186/data/Hong_Kong/Legislative_Council/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/288_Hong-Kong_Legislative-Council_Sixth-Legislative-Council_100persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/328a37393a77ca15901910bbc71e21bee9d403c2/data/Hungary/Assembly/term-41.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/290_Hungary_Országgyűlés_2018–_245persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/ff590ca3075989270999a0d31620f762ffbfff9c/data/Iceland/Assembly/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/292_Iceland_Alþingi_Alþingi-2017–_243persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/d042af93644b9b2481c5f01f4c0e5ba01565a19f/data/India/Lok_Sabha/term-16.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/300_India_Lok-Sabha_16th-Lok-Sabha_541persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/c0789c33f95b48b0424d1811115631102b814951/data/Indonesia/Council/term-18.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/301_Indonesia_Dewan-Perwakilan-Rakyat_The-Indonesian-House---11th-General-Election_662persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Iran/Assembly/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/302_Iran_Majles_10th-Assembly_508persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Iraq/Majlis/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/304_Iraq_Council-of-Representatives_2nd-Council_328persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/da756ac796e511a8e342b3af3bc4288f1b6fd17c/data/Ireland/Dail/term-32.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/305_Ireland_Dáil-Éireann_32nd-Dáil_657persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Isle_of_Man/House_of_Keys/term-2011.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/318_Isle-of-Man_House-of-Keys_2011–_29persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/afe250cd6d25b0d9836a48cdcd8741e68290dc76/data/Italy/Senate/term-18.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/340_Italy_Senate_18th-Legislature-of-Italy_566persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/6afc080d1a3534565ce9566d890b22141976de59/data/Jamaica/House_of_Representatives/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/342_Jamaica_House-of-Representatives_2016–_81persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/4997157b33f4f09b9480b278a32c64a18ee029e4/data/Japan/House_of_Representatives/term-48.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/344_Japan_Shūgiin_48th-House-of-Representatives_567persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Jersey/States/term-2011.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/346_Jersey_States_Assembly-2014–_62persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Jordan/House_of_Representatives/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/347_Jordan_House-of-Representatives_2013–_150persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Kazakhstan/Assembly/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/348_Kazakhstan_Mazhilis_Sixth-Convocation_171persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/5e06740f319f09b4d1b1821a15c539c1bfabd73a/data/Kenya/Assembly/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/350_Kenya_National-Assembly_11th-Parliament_355persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Kuwait/National_Assembly/term-14.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/358_Kuwait_National-Assembly_14th-Legislative-Session_45persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/95a68797bcc3d229c870f944c14e1b9adb34a76f/data/Kyrgyzstan/Council/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/359_Kyrgyzstan_Supreme-Council_6th-Convocation_209persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Laos/Assembly/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/361_Laos_National-Assembly_2016–_236persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/2f8bad6364f9f0a8a9e459812686e74ea7d2f002/data/Lesotho/Assembly/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/367_Lesotho_National-Assembly_10th-Parliament_185persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Liberia/House/term-53.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/369_Liberia_House-of-Representatives_53rd-Session_73persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Libya/House_of_Representatives/term-1.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/370_Libya_House-of-Representatives_1st-House-of-Representatives_189persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/efff54a7f6c02dc2d0a99333d0ada8234c68a1ad/data/Liechtenstein/Landtag/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/371_Liechtenstein_Landtag_2017-_62persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/0bd77f1ade0deaf0fd8b8c8088875b8b03f5f9f2/data/Luxembourg/Chamber/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/376_Luxembourg_Chamber-of-Deputies_2013–_63persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Madagascar/Assembly/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/379_Madagascar_National-Assembly_2013-2018_157persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/81362d4f718c0b403764f5322ae3521b797c752e/data/Malawi/Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/380_Malawi_National-Assembly_2014–_193persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/21d690a3a4cace2c74df57f4f13119e63b01119e/data/Maldives/Majlis/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/394_Maldives_Majlis_2014_85persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/980ac64235372064223ae8da06bb11575e412688/data/Mali/Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/395_Mali_National-Assembly_5th-Legislature_142persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/bddf723929029362890352be0a98dec0803f002f/data/Malta/Assembly/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/396_Malta_Parliament_13th-Parliament_93persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Mauritania/National_Assembly/term-12.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/399_Mauritania_National-Assembly_12th-National-Assembly_147persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Mauritius/National_Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/400_Mauritius_National-Assembly_10th-National-Assembly_62persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Micronesia/Congress/term-19.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/403_Micronesia_Congress_19th-Congress_24persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f7218f4de0f7b43e70b281439dd9fdd00e9c4e05/data/Moldova/Parlamentul/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/406_Moldova_Parlament_Legislatura-XX_129persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1ee10d9e9abbbb775d46dfb2d859347ce66ff9e2/data/Monaco/Council/term-2018.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/407_Monaco_National-Council_2018-2023_63persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/3b785c6b253d4a8b6390117add367243899cff53/data/Mongolia/Assembly/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/411_Mongolia_State-Great-Khural_State-Grand-Khural-2016-_160persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Montserrat/Assembly/term-1.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/415_Montserrat_Legislative-Assembly_1st-Legislative-Assembly_9persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/7dbaf073c7a3fb93fa653ddca9b25fbf2d71792c/data/Morocco/House/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/416_Morocco_House-of-Representatives_10th-Legislature_677persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Mozambique/Assembly/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/418_Mozambique_Assembleia-da-República_VIII-Legislature_250persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Nagorno_Karabakh/Assembly/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/420_Nagorno-Karabakh_National-Assembly_6th-Convocation_33persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/ba66e59d36e7fca1abd20aad089c3e6df0d3b6ae/data/Namibia/Assembly/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/421_Namibia_National-Assembly_6th-National-Assembly_270persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Namibia/Council/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/428_Namibia_National-Council_5th-National-Council_115persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Nauru/Parliament/term-22.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/433_Nauru_Parliament_22nd-Parliament_34persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/fc8a968a3c9d8bcea64fb7bb043588572a2000f3/data/Nepal/Assembly/term-ca2.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/437_Nepal_Constituent-Assembly_2nd-Constituent-Assembly_550persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1fcfe5be96d664c55df9c11332b1b1ed2d860457/data/Netherlands/House_of_Representatives/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/438_Netherlands_Tweede-Kamer_2017–_279persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e5dbf86fe3e86a5d58a6e1e3923339ed7744bf25/data/New_Caledonia/Congress/term-4.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/440_New-Caledonia_Congress_4e-mandat_56persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/71ae1af36f556da492455c5398ec0251d492efe4/data/New_Zealand/House/term-52.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/441_New-Zealand_New-Zealand-Parliament_52nd-New-Zealand-Parliament_266persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Nicaragua/Asamblea/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/446_Nicaragua_National-Assembly_2012–2017_90persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/69c10eb7bb7a426cde88c63244cbcb7790a30894/data/Nigeria/Representatives/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/448_Nigeria_House-of-Representatives_8th-National-Assembly-of-Nigeria_370persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/b13cb313dce452013ac724b8d39ac7d1caa3e3bc/data/Nigeria/Senate/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/449_Nigeria_Senate_8th-National-Assembly-of-Nigeria_115persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/07936233d36a566e502aa1679b56cd7d8333aa48/data/Niue/Assembly/term-16.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/450_Niue_Assembly_16th-Assembly_25persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/7bb5414cd492966a7714d440bb8d7f729e7b4f89/data/North_Korea/National_Assembly/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/453_North-Korea_Supreme-People’s-Assembly_13th-Assembly_687persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Northern_Cyprus/Assembly/term-14.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/454_Northern-Cyprus_Assembly-of-the-Republic_8th-Parliament_50persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/37300d3f18421e391838f11921ce5cefd8dbb6f3/data/Northern_Ireland/Assembly/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/455_Northern-Ireland_Northern-Ireland-Assembly_6th-Assembly_283persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/31a469101fd5d1b3c38cbc612746ac3930ccdd2e/data/Northern_Mariana_Islands/House/term-19.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/461_Northern-Mariana-Islands_House-of-Representatives_19th-Commonwealth-Legislature_20persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9798f45a445f36e67043744d57bcda5121d63ff7/data/Norway/Storting/term-2017-2021.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/462_Norway_Storting_2017-2021_1209persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Palau/House_of_Delegates/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/483_Palau_House-of-Delegates_Ninth-Olbiil-Era-Kelulau_16persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Panama/Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/484_Panama_Asamblea-Nacional_2014–_75persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Papua_New_Guinea/Parliament/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/485_Papua-New-Guinea_National-Parliament_2012-2017_111persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e8f23b6bc736d1fae1e59ecafb4cad935b50804d/data/Paraguay/Deputies/term-2018.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/486_Paraguay_Chamber-of-Deputies_2018–_137persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/a76837dc60d15671f955e568586ccc82b89bab89/data/Philippines/House/term-17.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/489_Philippines_House-of-Representatives_17th-Congress-of-the-Philippines_437persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Pitcairn/Island_Council/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/491_Pitcairn_Island-Council_2013–_12persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/337d82bea98adba0b36dabe5bae9979c49d9994b/data/Poland/Sejm/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/494_Poland_Sejm_8th-Polish-Sejm_2181persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/4f31692d86df4c0871b113fe0b9b24386e5ef30e/data/Portugal/Assembly/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/502_Portugal_Assembleia-da-República_13th-Portuguese-Assembly_2169persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/3ea06810cfa35a3114638fc9443333edd53b8f06/data/Puerto_Rico/House_of_Representatives/term-30.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/514_Puerto-Rico_House-of-Representatives_30th-House-of-Representatives-of-Puerto-Rico_74persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/c35e5c709196142468f08b40e204e5a4eb1abece/data/Romania/Deputies/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/516_Romania_Chamber-of-Deputies_2016–2020-legislature-of-the-Romanian-Parliament_648persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/df9b4b9e20db439c6c99bff385fcd2e38002ec47/data/Russia/Duma/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/518_Russia_Duma_7th-State-Duma-of-the-Russian-Federation_700persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Rwanda/Deputies/term-3.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/520_Rwanda_Chamber-of-Deputies_3rd-Legislature_77persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Saint_Helena/Legislative_Council/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/522_Saint-Helena_Legislative-Council_2013–_12persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/af7b618869df3c45fbedf8feea77705e1bd56581/data/Saint_Kitts_and_Nevis/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/523_Saint-Kitts-and-Nevis_National-Assembly_8th-National-Assembly_30persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Saint_Lucia/Assembly/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/531_Saint-Lucia_House-of-Assembly_10th-Parliament_30persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/cc827fd1ee8a5e7d3e76e1ef1a04daa1a8d440c0/data/Saint_Martin/Council/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/534_Saint-Martin_Territorial-Council_2012–_23persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Saint_Pierre_and_Miquelon/Territorial_Council/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/535_Saint-Pierre-and-Miquelon_Territorial-Council_3rd-Territorial-Council_43persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Saint_Vincent_and_the_Grenadines/Assembly/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/538_Saint-Vincent-and-the-Grenadines_House-of-Assembly_9th-Vincentian-Assembly_18persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Samoa/Parliament/term-16.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/540_Samoa_Fono_16th-Parliament_81persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/fa1423cacdfbb08822d87192a435ce6f61899bfb/data/San_Marino/Council/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/542_San-Marino_Grand-and-General-Council_Council-2016–_102persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Saudi_Arabia/Shura/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/548_Saudi-Arabia_Shura-Council_6th-Shura_148persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/24e1646a1828ea0c37f9363d356d3719d9d8d372/data/Scotland/Parliament/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/549_Scotland_Scottish-Parliament_5th-Parliament_305persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/b4f74ef24b609216a0ee4a36e784db35aa447992/data/Senegal/Assembly/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/554_Senegal_National-Assembly_2012–_149persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/48409b8f97b7c2f4520e71ae5fbff84d0b61dc8a/data/Serbia/National_Assembly/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/555_Serbia_National-Assembly_11th-Convocation-of-the-National-Assembly-of-the-Republic-of-Serbia_406persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Seychelles/Assembly/term-2011.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/557_Seychelles_National-Assembly_2011–2016_32persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Sierra_Leone/Parliament/term-2-4.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/558_Sierra-Leone_Parliament_Fourth-Parliament-of-the-Second-Republic_124persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e4524d4f44163ef0157bf0f362c1dc07fb324eb8/data/Singapore/Parliament/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/559_Singapore_Parliament_13th-Parliament_403persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/186d70cd07a5610d13673731f71a1d3dbc9fe30c/data/Sint_Maarten/Estates/term-4.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/572_Sint-Maarten_Estates_4th-Estates_29persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9d94719dd6f967a325eda1ff88db2008e72838a4/data/Slovakia/National_Council/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/575_Slovakia_National-Council_7th-Národná-rada_606persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9bf1af6d5882af6046e88b27c286aff4dc7c1ce0/data/Slovenia/National_Assembly/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/581_Slovenia_Državni-zbor_8th-National-Assembly_152persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/ab05af21cd84657f885afcfd2e2d75f9bda343a3/data/Solomon_Islands/Parliament/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/583_Solomon-Islands_National-Parliament_10th-Parliament-of-the-Solomon-Islands_51persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/475a3a7b0fbf499c9959e5915ebc206790b6eaf5/data/Somalia/Lower/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/584_Somalia_House-of-the-People_2012–_204persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Somaliland/Representatives/term-2005.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/585_Somaliland_House-of-Representatives_First-Parliament_82persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/aebf48fd27277b135d9495d661e9324cdf9b3579/data/South_Africa/Assembly/term-26.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/586_South-Africa_National-Assembly_26th-South-African-Parliament_497persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/6e4467efa11cd6df79f091eacf1bf3c2582513e4/data/South_Korea/National_Assembly/term-20.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/587_South-Korea_National-Assembly_20th-Legislative-Assembly_454persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/South_Ossetia/Parliament/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/589_South-Ossetia_Parliament_6th-Convocation_34persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/South_Sudan/Assembly/term-1.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/590_South-Sudan_National-Legislative-Assembly_1st-Parliament_167persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Sri_Lanka/Parliament/term-15.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/593_Sri-Lanka_Parliament_15th-Sri-Lankan-Parliament_228persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Suriname/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/594_Suriname_National-Assembly_2015–_87persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Swaziland/Assembly/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/596_Swaziland_House-of-Assembly_10th-Parliament_93persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/607c996472d65b5f3bcbaa8fc8c0b69abd415b8d/data/Sweden/Riksdag/term-2018.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/598_Sweden_Riksdag_2018–2022_1763persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/d9d69d7af5cca93f597e2f42e073263c42ff3b49/data/Switzerland/National_Council/term-50.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/611_Switzerland_National-Council_50.-Legislatur_1235persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/2da2fc3a9c1fa6664d0eb36fe2a78576b1211e8f/data/Taiwan/Legislative_Yuan/term-9.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/626_Taiwan_Legislative-Yuan_9th-Legislative-Yuan_170persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Tajikistan/Representatives/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/628_Tajikistan_Assembly-of-Representatives_2015–_62persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e54e042b376aea0bb3777bbaab4a4d605ef387ef/data/Tanzania/Assembly/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/629_Tanzania_National-Assembly_11th-Parliament-of-Tanzania_906persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/45e3c4d12c4ac4d7c161d0dc08a12dab34494a06/data/Thailand/National_Legislative_Assembly/term-2557.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/633_Thailand_National-Legislative-Assembly_2557-(NCPO)_273persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Timor_Leste/Parlamento/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/634_Timor-Leste_Parlamento_3rd-Parliament_65persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Togo/Assembly/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/635_Togo_National-Assembly_2013–_91persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/cb9cdcac9208a55b422e5f838c0fdf9f26c9e3ff/data/Tonga/Assembly/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/636_Tonga_Legislative-Assembly_2014-2018_27persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Transnistria/Supreme_Council/term-6.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/637_Transnistria_Supreme-Council_6th-Convocation_71persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Trinidad_and_Tobago/Representatives/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/639_Trinidad-and-Tobago_House-of-Representatives_11th-Republican-Parliament_42persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Trinidad_and_Tobago/Senate/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/640_Trinidad-and-Tobago_Senate_11th-Republican-Parliament_31persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1d7348ba7ed1cd10594ea3637883699abcbc3ede/data/Tunisia/Majlis/term-1.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/641_Tunisia_Assembly-of-the-Representatives-of-the-People_1st-Assembly-of-the-Representatives-of-the-People_226persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/65f97d1d9a83c0e2f48fb266fb2308958f1d61ad/data/Turkey/Assembly/term-26.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/642_Turkey_Grand-National-Assembly_26th-Parliament_6899persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Turkmenistan/Mejlis/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/668_Turkmenistan_Mejlis_5th-Convocation_237persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Turks_and_Caicos_Islands/Assembly/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/670_Turks-and-Caicos-Islands_House-of-Assembly_3rd-Assembly_22persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/5c1f1e3a8430c64dfa4bcc84de61ce47ad4bcf3d/data/Tuvalu/Parliament/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/672_Tuvalu_Parliament_11th-Parliament_27persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/57725800fb48504ec118c3866edf9d79e967f101/data/Uganda/Parliament/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/676_Uganda_Parliament_10th-Parliament_651persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/4176292ed2dfd7855b4e5d72616e41e5edb95cd5/data/Ukraine/Verkhovna_Rada/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/678_Ukraine_Verkhovna-Rada_8th-Verkhovna-Rada_466persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/ee0feea9035346a1f9091f2211527107317f007f/data/UK/Commons/term-57.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/680_United-Kingdom_House-of-Commons_57th-Parliament-of-the-United-Kingdom_1436persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1edd14dfd8ce60f3f613810ed1ed8195f9f4b6ce/data/United_States_of_America/House/term-115.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/686_United-States-of-America_House-of-Representatives_115th-Congress_1648persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/5d45092705c2ac36f34e4519c717953c49d29c46/data/United_States_of_America/Senate/term-115.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/705_United-States-of-America_Senate_115th-Congress_321persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/1d428a3ec7793b08ee425614ce271700c1e7d9d6/data/Uruguay/Deputies/term-48.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/724_Uruguay_Chamber-of-Deputies_XLVIII-Legislatura_125persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Uzbekistan/Legislative_Chamber/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/725_Uzbekistan_Legislative-Chamber_5th-Convocation_147persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/71d16bcca418914c64f448cfbf027e65e86015f9/data/Vanuatu/Parliament/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/726_Vanuatu_Parliament_11th-Parliament_96persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Vatican_City/Pontifical_Commission/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/728_Vatican-City_Pontifical-Commission_2016–_8persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/59f7cb92796e9bd593d362f19e01fef7877b9ec9/data/Venezuela/Assembly/term-4.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/730_Venezuela_National-Assembly_4th-National-Assembly-of-Venezuela_279persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/153c59732e1ff83491cd09251955656b057c3453/data/Wales/Assembly/term-5.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/733_Wales_National-Assembly-for-Wales_5th-Welsh-Assembly_141persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Wallis_and_Futuna/Territorial_Assembly/term-2017.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/738_Wallis-and-Futuna_Territorial-Assembly_2017–_29persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Yemen/Majlis/term-2003.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/740_Yemen_House-of-Representatives_2003–_302persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e14ab11cec6d6cf941290657b94163bc5aaac025/data/Zambia/Assembly/term-2016.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/741_Zambia_National-Assembly_12th-Assembly_260persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f74be3db4c76d1b42155ad56fa63cad6b3ed2bdd/data/Zimbabwe/Assembly/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/743_Zimbabwe_House-of-Assembly_8th-Parliament_229persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Zimbabwe/Senate/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Open/744_Zimbabwe_Senate_8th-Parliament_67persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)



print('ready')


#Closed term
url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f93ecce20a907eff09517de00d970968d18b9fc3/data/Montenegro/Assembly/term-25.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/414_Montenegro_Skupština_Skupština-2012-_82persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Falkland_Islands/Assembly/term-2013.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/172_Falkland-Islands_Legislative-Assembly_2013–2017_49persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/a1e6e91c50e622ece73b7db1d1c44b9beb8dd44b/data/Haiti/Deputies/term-2011.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/286_Haiti_Chamber-of-Deputies_2011-2015_99persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Brunei/Legislative_Council/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/94_Brunei_Legislative-Council-of-Brunei_11th-Legislative-Council_19persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Norfolk_Island/Assembly/term-14.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/452_Norfolk-Island_Legislative-Assembly_14th-Legislative-Assembly_9persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e86c029543edaf205cf6bf711d97c972bfd935f0/data/Afghanistan/Wolesi_Jirga/term-2010.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/2_Afghanistan_Wolesi-Jirga_2010–2015_258persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f579b006c9b302a0da444a9d58cbda474cb84d89/data/United_Arab_Emirates/Federal_National_Council/term-2011.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/679_United-Arab-Emirates_Federal-National-Council_2011–2015_40persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/e127654ae12a5d968bc5555405aa25440817a48e/data/Gibraltar/Parliament/term-12.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/234_Gibraltar_Parliament_Twelfth-Gibraltar-Parliament_83persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Oman/Majlis/term-7.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/481_Oman_Majlis-al-Shura_7th-Period_84persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Kiribati/Parliament/term-10.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/351_Kiribati_Parliament_10th-Parliament_62persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Marshall_Islands/Nitijela/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/398_Marshall-Islands_Nitijela_2012–2015_33persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/Myanmar/House_of_Representatives/term-1.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/419_Myanmar_House-of-Representatives_1st-Pyithu-Hluttaw_314persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Niger/Assembly/term-7.1.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/447_Niger_Assemblée-nationale_1ere-Legislature-de-la-7eme-Republique_113persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f661a8bb11fb6d9d5e34470c0a099b7f354078de/data/Syria/Majlis/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/625_Syria_Peoples-Council_Peoples-Council-of-Syria-2012_274persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/a09496e3d2074a17f9b2940440d8ac23b51fc21a/data/Spain/Congress/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/591_Spain_Congreso-de-los-Diputados_11th-legislature-of-Spain_621persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/8fa68d527dc47a0526f537f3c8546b102421b9fd/data/Israel/Knesset/term-20.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/319_Israel_Knesset_Knesset-20_933persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/0931052added484f5012f44f7aaa1026bef2338f/data/Peru/Congreso/term-2011.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/488_Peru_Congress_Congress-of-2011–2016_127persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/c6c17be0104b663137564c38228535df5ce201cf/data/Dominican_Republic/Diputados/term-2010.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/166_Dominican-Republic_Chamber-of-Deputies_2010–2016_190persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/66e01d3fb391002362595627addcf3f22295ea63/data/Macedonia/Sobranie/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/378_Macedonia_Sobranie_Sobranie-2014-2016_92persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/7eb71fdef78a6f141b27e9a965894b5221d6d38d/data/Curacao/Estates/term-2.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/145_Curaçao_Estates-of-Curaçao_2nd-Curaçaoan-Parliament_21persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f6441cce7b66546581a36a9115cd6a4233544bd3/data/Lithuania/Seimas/term-11.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/375_Lithuania_Seimas_Eleventh-Seimas_150persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/554a6cb306153130ac5558e4c015471d63e57cb7/data/Sark/Chief_Pleas/term-2015.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/544_Sark_Chief-Pleas_2015-2016_46persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/9a75c94fb3f01a45e5616242dec9743ba96f137f/data/US_Virgin_Islands/Legislature/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/675_US-Virgin-Islands_Legislature_31st-Legislature_15persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/03073e4006c40a45dde3882562f0218c803c981b/data/Saint_Barthelemy/Council/term-2012.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/521_Saint-Barthélemy_Territorial-Council_2012–2017_19persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/7710d8478f6bed092c6939d40a6cc0df42b000f6/data/Kosovo/Assembly/term-chamber_2014-07-17.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/353_Kosovo_Kuvendit_Fifth-Legislative-Period_399persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/7f23530631a345599437d7fb89ee5cfcdf830bc7/data/Ecuador/Asamblea/term-2.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/167_Ecuador_National-Assembly_2nd-Legislative-Assembly-of-Ecuador_146persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/c9f59eb32104351c90e57db2360a3a53eebe43b4/data/Lebanon/Parliament/term-2009.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/366_Lebanon_Parliament_2009–2017_130persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/b1165a8640778910ca33bf65a2f36db1ba857fd6/data/Albania/Assembly/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/3_Albania_Kuvendi_VIII-Pluralist-Legislature_213persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/26669cf7743825fe786809688ffef2117d4fb9e0/data/Austria/Nationalrat/term-25.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/60_Austria_Nationalrat_25th-National-Council_182persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/94846bada009cf427850d283aa391cec97de4775/data/Argentina/Diputados/term-135.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/29_Argentina_Cámara-de-Diputados_135º_396persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/f287a99ed1b2277ed96635c5030f9c2be0545548/data/Chile/Deputies/term-8.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/125_Chile_National-Congress_Legislativo-2014-2018_375persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/bf5475a6badf4ad7dc61373378565558ea2ada16/data/Italy/House/term-17.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/339_Italy_House-of-Representatives_17th-Legislature-of-Italy_668persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/321ee3653ffa3443da804ee98f559cd398e197c4/data/Malaysia/Dewan_Rakyat/term-13.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/381_Malaysia_Dewan-Rakyat_13th-Malaysian-Parliament_1121persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/777b42a0dea981750b5744f16a36200d04acf6cf/data/Costa_Rica/Assembly/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/141_Costa-Rica_Legislative-Assembly_2014-2018-legislative-period_57persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/97f3265626ab0a7c49f4eaa3baf3e9e05585e741/data/Colombia/Representatives/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/133_Colombia_Cámara-de-Representantes_2014-2018-parliament_171persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/92487d3a8fb7154fb0cd5321a1c5eac648518296/data/Colombia/Senate/term-2014.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/134_Colombia_Senado_2014–2018_101persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/8896876ff4a131c92b5e70d4c8fa0279d49cf791/data/Pakistan/Assembly/term-14.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/482_Pakistan_National-Assembly_14th-National-Assembly_352persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/8ffa1824656f5ad93d6af18adc45d494230d6dba/data/Mexico/Deputies/term-63.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/401_Mexico_Chamber-of-Deputies_LXIII-Legislature-of-the-Mexican-Congress_1031persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)

url='https://cdn.rawgit.com/everypolitician/everypolitician-data/735bd51cd6905a3ac9fb662175a9f02a0f21626c/data/Latvia/Saeima/term-12.csv'
response = requests.get(url, stream=True)
response.raise_for_status()

with open('Everypolitician_Closed/363_Latvia_Saeima_12th-Saeima_213persons.csv', 'wb') as handle:
	for block in response.iter_content(1024):
		handle.write(block)


print('ready')



data=[
['Abkhazia','1_Abkhazia_Peoples-Assembly_5th-Convocation_35persons.csv'],
['Åland','745_Åland_Lagting_2015–2019_60persons.csv'],
['Alderney','5_Alderney_States_2017-2018_14persons.csv'],
['Algeria','7_Algeria_Peoples-National-Assembly_7th-National-Assembly_478persons.csv'],
['American Samoa','8_American-Samoa_House-of-Representatives_2015–_17persons.csv'],
['Andorra','9_Andorra_Consell-General_2015_31persons.csv'],
['Angola','10_Angola_National-Assembly_4th-National-Assembly_343persons.csv'],
['Anguilla','12_Anguilla_House-of-Assembly_2015–2020_18persons.csv'],
['Antigua and Barbuda','19_Antigua-and-Barbuda_House-of-Representatives_2014–2019_63persons.csv'],
['Armenia','35_Armenia_National-Assembly_6th-Convocation_186persons.csv'],
['Aruba','37_Aruba_Estates-of-Aruba_7th-Aruban-Estates_21persons.csv'],
['Australia','38_Australia_House-of-Representatives_45th-Parliament_514persons.csv'],
['Australia','49_Australia_Senate_45th-Parliament_270persons.csv'],
['Azerbaijan','61_Azerbaijan_National-Assembly_5th-Convocation-of-the-National-Assembly-of-Azerbaijan_152persons.csv'],
['Bahamas','63_Bahamas_House-of-Assembly_2012–_38persons.csv'],
['Bahrain','64_Bahrain_Council-of-Representatives_2014–_40persons.csv'],
['Bangladesh','65_Bangladesh_Jatiyo-Sangshad_10th-Parliament_550persons.csv'],
['Barbados','67_Barbados_House-of-Assembly_2013–2018_30persons.csv'],
['Belarus','68_Belarus_House-of-Representatives_6th-Convocation_192persons.csv'],
['Belgium','70_Belgium_Chamber-of-Representatives_54e-législature_178persons.csv'],
['Belize','71_Belize_House-of-Representatives_10th-House-of-Representatives_41persons.csv'],
['Benin','73_Benin_Assemblée-Nationale_7è-législature_83persons.csv'],
['Bermuda','74_Bermuda_Parliament_2017–_48persons.csv'],
['Bhutan','76_Bhutan_National-Assembly_2nd-Parliament_48persons.csv'],
['Bolivia','77_Bolivia_Chamber-of-Deputies_2015–_130persons.csv'],
['Bosnia and Herzegovina','78_Bosnia-and-Herzegovina_House-of-Representatives_2014–_42persons.csv'],
['Botswana','79_Botswana_National-Assembly_11th-Parliament-of-Botswana_64persons.csv'],
['Brazil','80_Brazil_Chamber-of-Deputies_55th-Chamber-of-Deputies-of-Brazil_942persons.csv'],
['British Virgin Islands','82_British-Virgin-Islands_House-of-Assembly_3rd-Assembly_23persons.csv'],
['Bulgaria','95_Bulgaria_National-Assembly_44th-National-Assembly_1106persons.csv'],
['Burkina Faso','101_Burkina-Faso_Assemblée-Nationale_VIIe-législature_476persons.csv'],
['Burundi','106_Burundi_Assemblée-nationale_2015–_227persons.csv'],
['Cabo Verde','108_Cabo-Verde_Assembleia-Nacional_IX-Legislatura_131persons.csv'],
['Cambodia','110_Cambodia_National-Assembly_5th-Mandate_123persons.csv'],
['Cameroon','111_Cameroon_Assemblée-Nationale_9e-législature_964persons.csv'],
['Cameroon','119_Cameroon_Sénat_9e-législature_100persons.csv'],
['Canada','120_Canada_House-of-Commons_42nd-Canadian-Parliament_550persons.csv'],
['Canada','122_Canada_Senate_42nd-Parliament_116persons.csv'],
['Cayman Islands','123_Cayman-Islands_Legislative-Assembly_2013–2017_18persons.csv'],
['Chad','124_Chad_Assemblée-Nationale_Troisième-Législature_191persons.csv'],
['China','132_China_National-People’s-Congress_12th-National-People’s-Congress_2956persons.csv'],
['Comoros','135_Comoros_Assembly-of-the-Union_2015–_24persons.csv'],
['Congo-Brazzaville','136_Congo-Brazzaville_Assemblee-Nationale_13ème-législature_122persons.csv'],
['Congo-Kinshasa (DRC)','137_Congo-Kinshasa-(DRC)_National-Assembly_2012–_498persons.csv'],
['Cook Islands','138_Cook-Islands_Parliament_14th-Parliament_49persons.csv'],
['Côte dIvoire','155_Côte-dIvoire_National-Assembly_2e-législature-de-la-2e-République_241persons.csv'],
['Croatia','142_Croatia_Sabor_9th-Sabor_354persons.csv'],
['Cyprus','146_Cyprus_House-of-Representatives_11th-Term_91persons.csv'],
['Czech Republic','148_Czech-Republic_Chamber-of-Deputies_7.-volební-období_892persons.csv'],
['Denmark','156_Denmark_Folketing_Folketing-2015–_613persons.csv'],
['Djibouti','164_Djibouti_National-Assembly_6th-National-Assembly_65persons.csv'],
['Dominica','165_Dominica_House-of-Assembly_2014–_21persons.csv'],
['Egypt','168_Egypt_Parliament_2015–_600persons.csv'],
['El Salvador','169_El-Salvador_Legislative-Assembly_2015–2018_84persons.csv'],
['Estonia','170_Estonia_Riigikogu_13th-Riigikogu_214persons.csv'],
['Faroe Islands','182_Faroe-Islands_Løgting_Løgting-2015–_103persons.csv'],
['Fiji','190_Fiji_Parliament_2014_54persons.csv'],
['Finland','191_Finland_Eduskunta_37th-Parliament-of-Finland_500persons.csv'],
['France','204_France_Assemblée-nationale_XVe-législature-de-la-Ve-République_1552persons.csv'],
['French Polynesia','208_French-Polynesia_Assembly_2013–_59persons.csv'],
['Gabon','209_Gabon_Assemblée-nationale_12-Législature_114persons.csv'],
['Gambia','210_Gambia_National-Assembly_2012–_53persons.csv'],
['Georgia','211_Georgia_Parliament-of-Georgia_9th-Convocation_249persons.csv'],
['Germany','213_Germany_Bundestag_19th-Bundestag_4073persons.csv'],
['Ghana','232_Ghana_Parliament_Seventh-Parliament-of-the-Fourth-Republic_413persons.csv'],
['Greece','246_Greece_Hellenic-Parliament_17th-Hellenic-Parliament_1783persons.csv'],
['Greenland','263_Greenland_Inatsisartut_Inatsisartut-12_161persons.csv'],
['Grenada','275_Grenada_House-of-Representatives_2013_15persons.csv'],
['Guam','276_Guam_Parliament_33rd-Guam-Legislature_28persons.csv'],
['Guatemala','280_Guatemala_Congress_8th-legislature-of-Guatemala_255persons.csv'],
['Guernsey','282_Guernsey_States_2016–_68persons.csv'],
['Guinea-Bissau','284_Guinea-Bissau_National-Peoples-Assembly_2014-_100persons.csv'],
['Guyana','285_Guyana_National-Assembly_11th-Parliament_64persons.csv'],
['Honduras','287_Honduras_National-Congress_VIII-Legislatura_131persons.csv'],
['Hong Kong','288_Hong-Kong_Legislative-Council_Sixth-Legislative-Council_100persons.csv'],
['Hungary','290_Hungary_Országgyűlés_2018–_245persons.csv'],
['Iceland','292_Iceland_Alþingi_Alþingi-2017–_243persons.csv'],
['India','300_India_Lok-Sabha_16th-Lok-Sabha_541persons.csv'],
['Indonesia','301_Indonesia_Dewan-Perwakilan-Rakyat_The-Indonesian-House---11th-General-Election_662persons.csv'],
['Iran','302_Iran_Majles_10th-Assembly_508persons.csv'],
['Iraq','304_Iraq_Council-of-Representatives_2nd-Council_328persons.csv'],
['Ireland','305_Ireland_Dáil-Éireann_32nd-Dáil_657persons.csv'],
['Isle of Man','318_Isle-of-Man_House-of-Keys_2011–_29persons.csv'],
['Italy','340_Italy_Senate_18th-Legislature-of-Italy_566persons.csv'],
['Jamaica','342_Jamaica_House-of-Representatives_2016–_81persons.csv'],
['Japan','344_Japan_Shūgiin_48th-House-of-Representatives_567persons.csv'],
['Jersey','346_Jersey_States_Assembly-2014–_62persons.csv'],
['Jordan','347_Jordan_House-of-Representatives_2013–_150persons.csv'],
['Kazakhstan','348_Kazakhstan_Mazhilis_Sixth-Convocation_171persons.csv'],
['Kenya','350_Kenya_National-Assembly_11th-Parliament_355persons.csv'],
['Kuwait','358_Kuwait_National-Assembly_14th-Legislative-Session_45persons.csv'],
['Kyrgyzstan','359_Kyrgyzstan_Supreme-Council_6th-Convocation_209persons.csv'],
['Laos','361_Laos_National-Assembly_2016–_236persons.csv'],
['Lesotho','367_Lesotho_National-Assembly_10th-Parliament_185persons.csv'],
['Liberia','369_Liberia_House-of-Representatives_53rd-Session_73persons.csv'],
['Libya','370_Libya_House-of-Representatives_1st-House-of-Representatives_189persons.csv'],
['Liechtenstein','371_Liechtenstein_Landtag_2017-_62persons.csv'],
['Luxembourg','376_Luxembourg_Chamber-of-Deputies_2013–_63persons.csv'],
['Madagascar','379_Madagascar_National-Assembly_2013-2018_157persons.csv'],
['Malawi','380_Malawi_National-Assembly_2014–_193persons.csv'],
['Maldives','394_Maldives_Majlis_2014_85persons.csv'],
['Mali','395_Mali_National-Assembly_5th-Legislature_142persons.csv'],
['Malta','396_Malta_Parliament_13th-Parliament_93persons.csv'],
['Mauritania','399_Mauritania_National-Assembly_12th-National-Assembly_147persons.csv'],
['Mauritius','400_Mauritius_National-Assembly_10th-National-Assembly_62persons.csv'],
['Micronesia','403_Micronesia_Congress_19th-Congress_24persons.csv'],
['Moldova','406_Moldova_Parlament_Legislatura-XX_129persons.csv'],
['Monaco','407_Monaco_National-Council_2018-2023_63persons.csv'],
['Mongolia','411_Mongolia_State-Great-Khural_State-Grand-Khural-2016-_160persons.csv'],
['Montserrat','415_Montserrat_Legislative-Assembly_1st-Legislative-Assembly_9persons.csv'],
['Morocco','416_Morocco_House-of-Representatives_10th-Legislature_677persons.csv'],
['Mozambique','418_Mozambique_Assembleia-da-República_VIII-Legislature_250persons.csv'],
['Nagorno-Karabakh','420_Nagorno-Karabakh_National-Assembly_6th-Convocation_33persons.csv'],
['Namibia','421_Namibia_National-Assembly_6th-National-Assembly_270persons.csv'],
['Namibia','428_Namibia_National-Council_5th-National-Council_115persons.csv'],
['Nauru','433_Nauru_Parliament_22nd-Parliament_34persons.csv'],
['Nepal','437_Nepal_Constituent-Assembly_2nd-Constituent-Assembly_550persons.csv'],
['Netherlands','438_Netherlands_Tweede-Kamer_2017–_279persons.csv'],
['New Caledonia','440_New-Caledonia_Congress_4e-mandat_56persons.csv'],
['New Zealand','441_New-Zealand_New-Zealand-Parliament_52nd-New-Zealand-Parliament_266persons.csv'],
['Nicaragua','446_Nicaragua_National-Assembly_2012–2017_90persons.csv'],
['Nigeria','448_Nigeria_House-of-Representatives_8th-National-Assembly-of-Nigeria_370persons.csv'],
['Nigeria','449_Nigeria_Senate_8th-National-Assembly-of-Nigeria_115persons.csv'],
['Niue','450_Niue_Assembly_16th-Assembly_25persons.csv'],
['North Korea','453_North-Korea_Supreme-People’s-Assembly_13th-Assembly_687persons.csv'],
['Northern Cyprus','454_Northern-Cyprus_Assembly-of-the-Republic_8th-Parliament_50persons.csv'],
['Northern Ireland','455_Northern-Ireland_Northern-Ireland-Assembly_6th-Assembly_283persons.csv'],
['Northern Mariana Islands','461_Northern-Mariana-Islands_House-of-Representatives_19th-Commonwealth-Legislature_20persons.csv'],
['Norway','462_Norway_Storting_2017-2021_1209persons.csv'],
['Palau','483_Palau_House-of-Delegates_Ninth-Olbiil-Era-Kelulau_16persons.csv'],
['Panama','484_Panama_Asamblea-Nacional_2014–_75persons.csv'],
['Papua New Guinea','485_Papua-New-Guinea_National-Parliament_2012-2017_111persons.csv'],
['Paraguay','486_Paraguay_Chamber-of-Deputies_2018–_137persons.csv'],
['Philippines','489_Philippines_House-of-Representatives_17th-Congress-of-the-Philippines_437persons.csv'],
['Pitcairn','491_Pitcairn_Island-Council_2013–_12persons.csv'],
['Poland','494_Poland_Sejm_8th-Polish-Sejm_2181persons.csv'],
['Portugal','502_Portugal_Assembleia-da-República_13th-Portuguese-Assembly_2169persons.csv'],
['Puerto Rico','514_Puerto-Rico_House-of-Representatives_30th-House-of-Representatives-of-Puerto-Rico_74persons.csv'],
['Romania','516_Romania_Chamber-of-Deputies_2016–2020-legislature-of-the-Romanian-Parliament_648persons.csv'],
['Russia','518_Russia_Duma_7th-State-Duma-of-the-Russian-Federation_700persons.csv'],
['Rwanda','520_Rwanda_Chamber-of-Deputies_3rd-Legislature_77persons.csv'],
['Saint Helena','522_Saint-Helena_Legislative-Council_2013–_12persons.csv'],
['Saint Kitts and Nevis','523_Saint-Kitts-and-Nevis_National-Assembly_8th-National-Assembly_30persons.csv'],
['Saint Lucia','531_Saint-Lucia_House-of-Assembly_10th-Parliament_30persons.csv'],
['Saint Martin','534_Saint-Martin_Territorial-Council_2012–_23persons.csv'],
['Saint Pierre and Miquelon','535_Saint-Pierre-and-Miquelon_Territorial-Council_3rd-Territorial-Council_43persons.csv'],
['Saint Vincent and the Grenadines','538_Saint-Vincent-and-the-Grenadines_House-of-Assembly_9th-Vincentian-Assembly_18persons.csv'],
['Samoa','540_Samoa_Fono_16th-Parliament_81persons.csv'],
['San Marino','542_San-Marino_Grand-and-General-Council_Council-2016–_102persons.csv'],
['Saudi Arabia','548_Saudi-Arabia_Shura-Council_6th-Shura_148persons.csv'],
['Scotland','549_Scotland_Scottish-Parliament_5th-Parliament_305persons.csv'],
['Senegal','554_Senegal_National-Assembly_2012–_149persons.csv'],
['Serbia','555_Serbia_National-Assembly_11th-Convocation-of-the-National-Assembly-of-the-Republic-of-Serbia_406persons.csv'],
['Seychelles','557_Seychelles_National-Assembly_2011–2016_32persons.csv'],
['Sierra Leone','558_Sierra-Leone_Parliament_Fourth-Parliament-of-the-Second-Republic_124persons.csv'],
['Singapore','559_Singapore_Parliament_13th-Parliament_403persons.csv'],
['Sint Maarten','572_Sint-Maarten_Estates_4th-Estates_29persons.csv'],
['Slovakia','575_Slovakia_National-Council_7th-Národná-rada_606persons.csv'],
['Slovenia','581_Slovenia_Državni-zbor_8th-National-Assembly_152persons.csv'],
['Solomon Islands','583_Solomon-Islands_National-Parliament_10th-Parliament-of-the-Solomon-Islands_51persons.csv'],
['Somalia','584_Somalia_House-of-the-People_2012–_204persons.csv'],
['Somaliland','585_Somaliland_House-of-Representatives_First-Parliament_82persons.csv'],
['South Africa','586_South-Africa_National-Assembly_26th-South-African-Parliament_497persons.csv'],
['South Korea','587_South-Korea_National-Assembly_20th-Legislative-Assembly_454persons.csv'],
['South Ossetia','589_South-Ossetia_Parliament_6th-Convocation_34persons.csv'],
['South Sudan','590_South-Sudan_National-Legislative-Assembly_1st-Parliament_167persons.csv'],
['Sri Lanka','593_Sri-Lanka_Parliament_15th-Sri-Lankan-Parliament_228persons.csv'],
['Suriname','594_Suriname_National-Assembly_2015–_87persons.csv'],
['Swaziland','596_Swaziland_House-of-Assembly_10th-Parliament_93persons.csv'],
['Sweden','598_Sweden_Riksdag_2018–2022_1763persons.csv'],
['Switzerland','611_Switzerland_National-Council_50.-Legislatur_1235persons.csv'],
['Taiwan','626_Taiwan_Legislative-Yuan_9th-Legislative-Yuan_170persons.csv'],
['Tajikistan','628_Tajikistan_Assembly-of-Representatives_2015–_62persons.csv'],
['Tanzania','629_Tanzania_National-Assembly_11th-Parliament-of-Tanzania_906persons.csv'],
['Thailand','633_Thailand_National-Legislative-Assembly_2557-(NCPO)_273persons.csv'],
['Timor Leste','634_Timor-Leste_Parlamento_3rd-Parliament_65persons.csv'],
['Togo','635_Togo_National-Assembly_2013–_91persons.csv'],
['Tonga','636_Tonga_Legislative-Assembly_2014-2018_27persons.csv'],
['Transnistria','637_Transnistria_Supreme-Council_6th-Convocation_71persons.csv'],
['Trinidad and Tobago','639_Trinidad-and-Tobago_House-of-Representatives_11th-Republican-Parliament_42persons.csv'],
['Trinidad and Tobago','640_Trinidad-and-Tobago_Senate_11th-Republican-Parliament_31persons.csv'],
['Tunisia','641_Tunisia_Assembly-of-the-Representatives-of-the-People_1st-Assembly-of-the-Representatives-of-the-People_226persons.csv'],
['Turkey','642_Turkey_Grand-National-Assembly_26th-Parliament_6899persons.csv'],
['Turkmenistan','668_Turkmenistan_Mejlis_5th-Convocation_237persons.csv'],
['Turks and Caicos Islands','670_Turks-and-Caicos-Islands_House-of-Assembly_3rd-Assembly_22persons.csv'],
['Tuvalu','672_Tuvalu_Parliament_11th-Parliament_27persons.csv'],
['Uganda','676_Uganda_Parliament_10th-Parliament_651persons.csv'],
['Ukraine','678_Ukraine_Verkhovna-Rada_8th-Verkhovna-Rada_466persons.csv'],
['United Kingdom','680_United-Kingdom_House-of-Commons_57th-Parliament-of-the-United-Kingdom_1436persons.csv'],
['United States of America','686_United-States-of-America_House-of-Representatives_115th-Congress_1648persons.csv'],
['United States of America','705_United-States-of-America_Senate_115th-Congress_321persons.csv'],
['Uruguay','724_Uruguay_Chamber-of-Deputies_XLVIII-Legislatura_125persons.csv'],
['Uzbekistan','725_Uzbekistan_Legislative-Chamber_5th-Convocation_147persons.csv'],
['Vanuatu','726_Vanuatu_Parliament_11th-Parliament_96persons.csv'],
['Vatican City','728_Vatican-City_Pontifical-Commission_2016–_8persons.csv'],
['Venezuela','730_Venezuela_National-Assembly_4th-National-Assembly-of-Venezuela_279persons.csv'],
['Wales','733_Wales_National-Assembly-for-Wales_5th-Welsh-Assembly_141persons.csv'],
['Wallis and Futuna','738_Wallis-and-Futuna_Territorial-Assembly_2017–_29persons.csv'],
['Yemen','740_Yemen_House-of-Representatives_2003–_302persons.csv'],
['Zambia','741_Zambia_National-Assembly_12th-Assembly_260persons.csv'],
['Zimbabwe','743_Zimbabwe_House-of-Assembly_8th-Parliament_229persons.csv'],
['Zimbabwe','744_Zimbabwe_Senate_8th-Parliament_67persons.csv']
]


politiciansDataFrame = pd.DataFrame([{'id':'',   
                             'country':'',         
                             'name':'',
                             'email': '',
                             'twitter': '',
                             'facebook': '',
                             'gender': '',
                             'area': '',
                             'wikidata': '',
                             'group': '',
                             'searchterm':'',         
                             'start_date': '',
                             'end_date': ''}])



for item in data:
    country=item[0]
    file='Everypolitician_Closed/'+item[1]

    with open(file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            searchterm=''
            id=''
            name=''
            email=''
            twitter=''
            facebook=''
            gender=''
            area=''
            wikidata=''
            group=''
            start_date=''
            end_date=''
            id=row["id"]
            name=row["name"]
            email=row["email"]
            twitter=row["twitter"]
            facebook=row["facebook"]
            gender=row["gender"]
            area=row["area"]
            wikidata=row["wikidata"]
            group=row["group"]
            start_date=row["start_date"] 
            end_date=row["end_date"]            
            searchterm=country + ' Politician ' + name
            politiciansDataFrame = politiciansDataFrame.append([{'id':id,
                                                         'country':country,
                                                         'name':name,
                                                         'email':email,                                                
                                                         'twitter':twitter,
                                                         'facebook': facebook,
                                                         'gender': gender,
                                                         'area': area,
                                                         'wikidata': wikidata,
                                                         'group': group,
                                                         'start_date': start_date,
                                                         'end_date': end_date,
                                                         'searchterm':searchterm}], ignore_index=True)

print('data ready')
csvfile='Everypolitician_Closed/names.csv'
politiciansDataFrame.to_csv(csvfile, columns=['searchterm'], quoting=csv.QUOTE_ALL, encoding='utf-8', index=False)
print('csv ready')

filename='Everypolitician_Closed/Everypolitician_politicians_data.xlsx'
writer = pd.ExcelWriter(filename, engine='xlsxwriter')
politiciansDataFrame.to_excel(writer, sheet_name='Sheet1')
writer.save()  
print('xls ready')    