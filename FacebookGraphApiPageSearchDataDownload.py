import requests
import os
import pandas as pd
import xlsxwriter
import csv
import json
import time
import datetime


resultsDataFrame = pd.DataFrame([{'id': 0,
                                'country': '',
                                'nameinresult': ''}])
with open('ongoingterm_missed2.csv', 'r') as csvfile:    
    adatok = list(csv.DictReader(csvfile))
for line in adatok:
    #print(line)
    resultId=line['id']
    country=line['country']
    nameinresult=line['name']
    resultsDataFrame = resultsDataFrame.append([{'id': resultId,
                                                 'country':country,
                                                 'nameinresult':nameinresult}], ignore_index=True)    
csvfile.close()


access_token='ACCESS TOKEN HERE'
a=0
errorDataFrame = pd.DataFrame([{'a_num': 0,
                                'b_num': '',
                                'nameinresult': '',
                                'resultid':'',
                                'c_status_code': 'status_code',
                                'd_type': 'type',
                                'e_code': 'code',
                                'f_subcode': 'subcode',
                                'g_message': 'message'}])
fbDataFrame = pd.DataFrame([{'id':'',
                             'searchcountry':'',                                                
                             'name':'',
                             'resultid':'',
                             'city': '',
                             'country': '',
                             'latitude': '',
                             'longitude': '',
                             'street': '',
                             'zip': '',
                             'link': ''}])
print('ready')


first=datetime.datetime.now()
for index, row in resultsDataFrame.iterrows():
    #print(row['id'], row['nameinresult'])
    resultid=''
    searchcountry=''
    nameinresult=''
    nameinresult=row['nameinresult']
    searchcountry=row['country']
    resultid=row['id']
    query=searchcountry + " " + nameinresult
    a=a+1
    if(a%1000==0):
        print(a)
    prmtrs = {'access_token' : access_token,
          'q' : query,
          'fields' : 'id',
          'limit':'100'}
    url='https://graph.facebook.com/v3.1/pages/search'
    try:
        r = requests.get(url, prmtrs)
        rawdata = r.json()
        if 'error' in rawdata:
            c_status_code=''
            d_type=''
            e_code= ''
            f_subcode=''
            g_message=''
            print(rawdata)
            if 'message' in rawdata['error']:
                g_message=rawdata['error']['message']
            if 'type' in rawdata['error']:
                d_type= rawdata['error']['type']
            if 'code' in rawdata['error']:
                e_code=rawdata['error']['code']
                if e_code==4:
                    print('Sleeping for an hour from start.')
                    second=datetime.datetime.now()
                    resultsDataFrame = resultsDataFrame.append([{'id': resultid,
                                                 'country':searchcountry,
                                                 'nameinresult':nameinresult}], ignore_index=True)                     
                    time.sleep(4000-((second-first).total_seconds()))
                    print('Restart access tokens.')
                    first=datetime.datetime.now()            
            if 'error_subcode' in rawdata['error']:
                f_subcode=rawdata['error']['error_subcode']
            errorDataFrame = errorDataFrame.append([{'a_num': a,'nameinresult': nameinresult, 'resultid':resultid, 'c_status_code': str(r.status_code), 'e_code': e_code, 'f_subcode': f_subcode, 'g_message': g_message}], ignore_index=True)
        if 'data' in rawdata:
            data=rawdata['data']
            for datum in data:
                if 'id' in datum:
                    pageId=str(datum['id'])
                fbDataFrame = fbDataFrame.append([{'id': pageId,
                                                 'searchcountry':searchcountry,
                                                 'searchname':nameinresult,
                                                 'resultid':resultid}], ignore_index=True)
            if 'paging' in rawdata:
                while 'next' in rawdata['paging']:
                    nextPage=rawdata['paging']['next']
                    r = requests.get(rawdata['paging']['next'])
                    rawdata = r.json()
                    if 'error' in rawdata:
                        c_status_code=''
                        d_type=''
                        e_code= ''
                        f_subcode=''
                        g_message=''
                        if 'message' in rawdata['error']:
                            g_message=rawdata['error']['message']
                        if 'type' in rawdata['error']:
                            d_type= rawdata['error']['type']
                        if 'code' in rawdata['error']:
                            e_code=rawdata['error']['code']
                            if e_code==4:
                                print('Sleeping for an hour from start.')
                                second=datetime.datetime.now()
                                resultsDataFrame = resultsDataFrame.append([{'id': resultid,
                                                 'country':searchcountry,
                                                 'nameinresult':nameinresult}], ignore_index=True)                     
                                
                                time.sleep(4000-((second-first).total_seconds()))
                                print('Restart access tokens.')
                                first=datetime.datetime.now()            
                        if 'error_subcode' in rawdata['error']:
                            f_subcode=rawdata['error']['error_subcode']
                        errorDataFrame = errorDataFrame.append([{'a_num': i,'b_num': nextPage,'nameinresult': nameinresult,  'resultid':resultid, 'c_status_code': str(r.status_code), 'e_code': e_code, 'f_subcode': f_subcode, 'g_message': g_message}], ignore_index=True)
                    if 'data' in rawdata:
                        data=rawdata['data']
                        for datum in data:
                            if 'id' in datum:
                                pageId=str(datum['id'])
                            fbDataFrame = fbDataFrame.append([{'id': pageId,
                                                             'searchcountry':searchcountry,
                                                             'searchname':nameinresult,
                                                             'resultid':resultid}], ignore_index=True)


    except Exception as error:
        print(rawdata, error)

csvfile=filename_start+'_csv.csv'
fbDataFrame.to_csv(csvfile, quoting=csv.QUOTE_ALL, encoding='utf-8', escapechar='\\', index=False)
print('Kesz a csv')


filename_start=filename

csvfile=filename_start+'_csv.csv'
fbDataFrame.to_csv(csvfile, quoting=csv.QUOTE_ALL, encoding='utf-8', escapechar='\\', index=False)
print('Kesz a csv')

filename=filename_start+'_data.xlsx'
writer = pd.ExcelWriter(filename, engine='xlsxwriter')
fbDataFrame.to_excel(writer, sheet_name='Sheet1')
writer.save()
filename=filename_start+'data_error.xlsx'
writer = pd.ExcelWriter(filename, engine='xlsxwriter')
errorDataFrame.to_excel(writer, sheet_name='Sheet1')
writer.save()
print('Végeztem.')   
